const userService = require('../services/user.service')
const assert = require('assert')
const bcrypt = require('bcrypt-nodejs')
const config = require('../config')

// get users
exports.getUsers = (req, res, next) => {
    userService.getProfile(req.params.id)
        .then(data => res.status(data.status).json(data))
        .catch(err => res.status(err.status).json(err))
}

// add new user
exports.addUser = (req, res, next) => {
    const valid = !!req.body.email && !!req.body.password
    if (!valid) {
        res.status(400).json({
            status: 400,
            msg: "Incomplete request body"
        })
    }
    // TODO: validate age is > 18
    if (valid && req.body.password) {
        bcrypt.genSalt(config.saltRounds, (err, salt) => {
            bcrypt.hash(req.body.password.trim(), salt, null, (err, hash) => {
                req.body.password = hash;
                const user = {
                    email: req.body.email.trim(),
                    password: req.body.password.trim(),
                    accountStatus: "active",
                }
                userService.addUser(user)
                    .then(data => res.status(data.status).json(data))
                    .catch(err => res.status(err.status).json(err))
            })
        })
    }
}

// update an existing user
exports.updateUser = (req, res, next) => {
    // expected from body: [name: {fname, lname}, birthDate, email, password, enrolledIquibs]
    const valid = !!req.body.email && !!req.body.password
    if (!valid) {
        res.status(400).json({
            status: 400,
            msg: "Incomplete request body"
        })
    }
    const user = {
        id: req.params.id.trim(),
        email: req.body.email.trim(),
        password: req.body.password.trim(),
    }
    userService.updateUser(user)
        .then(
            data => {
                res.status(data.status).json(data)
            }
        )
        .catch(
            err => {
                res.status(err.status).json(err)
            }
        )
}

// get profile of authenticated user
exports.getProfile = (req, res, next) => {
    userService.getProfile(req.decoded._id)
        .then(
            data => {
                res.status(data.status).json(data)
            }
        )
        .catch(
            err => {
                res.status(err.status).json(err)
            }
        )
}

// teminate account of authenticated user
exports.terminateAccount = (req, res, next) => {
    userService.terminateAccount(req.params.id)
        .then(
            data => {
                res.status(data.status).json(data)
            }
        )
        .catch(
            err => {
                res.status(err.status).json(err)
            }
        )
}
