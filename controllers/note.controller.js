const noteService = require('../services/note.service')
const assert = require('assert')
const bcrypt = require('bcrypt-nodejs')
const config = require('../config')

exports.getNotes = (req, res, next) => {
    noteService.getNotes(req.decoded)
    .then(data => res.status(data.status).json(data))
    .catch (err => res.status(err.status).json(err))    
}

exports.deleteNote = (req, res, next) => {
    noteService.deleteNote(req.params.id, req.decoded)
    .then(data => res.status(data.status).json(data))
    .catch(err => {
        res.status(err.status).json(err)
    })    
}

exports.updateNote = (req, res, next) => {
    noteService.updateNote(req.params.id, req.body.note)
    .then(data => res.status(data.status).json(data))
    .catch (err => res.status(err.status).json(err)) 
}

exports.addNote = (req, res, next) => {
    req.body.author = req.decoded
    req.body.status = 'active'
    req.body.excerpt = ''
    if (req.body.body != null) {
	let words = req.body.body.split(" ")
	if (words.length > 4) {
		for (let i = 0; i <= 4; i++) {
			req.body.excerpt += words[i] + " "
		}
	} else {
		req.body.excerpt = req.body.body
	}
	req.body.excerpt += "..."
    }
    noteService.addNote(req.body)
    .then(data => res.status(data.status).json(data))
    .catch (err => res.status(err.status).json(err))    
}
