const Express = require('express')
const config = require('./config')
const mongoose = require('mongoose')
const bcrypt = require('bcrypt-nodejs')
// const helmet = require('helmet')
const bodyParser = require('body-parser')
const jwt = require('jsonwebtoken')


const app = Express()
app.set('secret', 'something secret')
app.set('secret', config.secret)
app.disable('x-powered-by')
// app.use(helmet())

const User = require('./models/user.model')
const Note = require('./models/note.model')
const userRoute = require('./routes/user.route')
const noteRoute = require('./routes/note.route')
const dbOptions = {
    autoReconnect: true,
    connectTimeoutMS: 9000
}
const connectDb = (options) => {
    mongoose.connect(config.database, options)
    // .then(() => { // if all is ok we will be here
    //     return server.start();
    // })
    // .catch(err => { // if error we will be here
    //     console.error('App starting error:', err.stack);
    //     process.exit(1);
    // });
}
connectDb(dbOptions)
mongoose.connection.on('disconnected', () => connectDb(dbOptions))
mongoose.connection.on('connected', () => console.log('connected to database'))
mongoose.connection.on('connecting', () => console.log('connecting to database...'))
mongoose.connection.on('reconnected', () => console.log('reconnected to database'))
// mongoose.connection.on('error', () => console.error.bind(console, 'connection error:'))

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*')
    res.header('Access-Control-Allow-Origin', '*')
    res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, OPTIONS');
    // res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Headers", "*");
    res.header("Cache-Control", "max-age=60")

    return next()
})

app.use('/api/v1/user', userRoute)
app.use('/api/v1/note', noteRoute)
// login
app.post('/api/v1/login', (req, res) => {
    User.findOne({
        email: req.body.email
    }, (err, user) => {
        if (err) {
            res.status(500).json({
                status: 500,
                error: err
            })
        } else if (user) {
            let password = {
                hash: user.password,
                plain: req.body.password
            }
            bcrypt.compare(password.plain, password.hash, (err, response) => {
                if (err) {
                    res.status(501).json({
			status: 501,
                        success: false,
                        message: "Internal server error"
                    })
                } else if (response) {
                    console.log(response)
                    const token = jwt.sign(user.toObject(), app.get('secret'), {
                        // expiresIn: 86400
                    })
                    res.status(200).json({
			status: 200,
                        success: true,
                        message: "Authentication success, token created",
                        token: token
                    })
                } else {
                    res.status(403).json({
			status: 403,
                        success: false,
                        message: "Authentication failed..."
                    })
                }
            })
            // if (user.password !== req.body.password) {
            //     res.status(403).json({
            //         success: false,
            //         message: "Authentication failed..."
            //     })
            // } else {
            //     const token = jwt.sign(user.toObject(), app.get('secret'), {
            //         expiresIn: 86400
            //     })
            //     res.status(200).json({
            //         success: true,
            //         message: "Authentication success, token created",
            //         token: token
            //     })
            // }
        } else {
            res.status(404).json({
		status: 404,
                success: false,
                message: "User not found"
            })
        }
    })
})

let p = process.env.PORT || 3000

const server = app.listen(p, () => {
    const host = server.address().address
    const port = server.address().port
    console.log(`app running on ${host}:${port}`);
})
