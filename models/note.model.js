const mongoose = require('mongoose')
const bcrypt = require('bcrypt-nodejs')
const config = require('../config')
const Schema = mongoose.Schema

var UserSchema = new Schema({
    title: {
        type: String,
        required: true
    },
    body: {
        type: String,
        required: true
    },
    author: {
        type: Object,
        required: true
    },
    excerpt: {
	    type: String
    },
    status: {
        type: String
    }
})

// set up a mongoose model
module.exports = mongoose.model('Note', UserSchema)
