const mongoose = require('mongoose')
const bcrypt = require('bcrypt-nodejs')
const config = require('../config')
const Schema = mongoose.Schema

var UserSchema = new Schema({
    name: {
        fname: {
            type: String
        },
        lname: {
            type: String
        }
    },
    avatarUrl: String,
    birthDate: {
        type: Date
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    notes: Array,
    password: {
        type: String,
        required: true
    },
    accountStatus: String,
    google: {
        id: String,
        token: String,
        email: String,
        name: String
    }
})

// UserSchema.pre('save', (next) => {
//     // if (this.isModified('password') || this.isNew) {
//         bcrypt.genSalt(config.saltRounds, (err, salt) => {
//             if (err) {
//                 return next(err)
//             }
//             console.log(this)
//             bcrypt.hash(this.password, salt, (err, hash) => {
//                 if (err) {
//                     return next(err)
//                 }
//                 this.password = hash
//                 next()
//             })
//         })
//     // } else {
//     //     return next()
//     // }
// })

UserSchema.methods.comparePassword = (passw, next) => {
    bcrypt.compare(passw, this.password, (err, isMatch) => {
        if (err) {
            return next(err)
        }
        next(null, isMatch)
    })
}


// set up a mongoose model
module.exports = mongoose.model('User', UserSchema)
