let mongoose = require("mongoose");
let Note = require('../models/note.model');
let fetch = require('node-fetch')
//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = "https://insync-api.herokuapp.com";
let should = chai.should();
let token = "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJub3RlcyI6W10sIl9pZCI6IjViMjgwNmYxMGY2ZDY5MDAxZTRlZTcwNiIsImVtYWlsIjoibWlsbGFAZ21haWwuY29tIiwicGFzc3dvcmQiOiIkMmEkMTAkL1FaL2JNdGk0b1NrZ0FOZW9rQWx4LmxsMnRRUDhXUzAvUTE2UTVTY0s0QkxFRFlGc3pNZy4iLCJhY2NvdW50U3RhdHVzIjoiYWN0aXZlIiwiX192IjowLCJpYXQiOjE1Mjk1OTc4OTV9.zP94lSTZLvJZrhGnZZkNH-L2_8kDq-enaaFTIisDbgM";

chai.use(chaiHttp);

describe('Middleware', () => {
    it('it should block /api/v1/note when token is not present', (done) => {
        chai.request(server)
                .get('/api/v1/note')
                .end((err, res) => {
                    res.should.have.status(403);
                    res.body.should.be.a('Object');
                    res.body.should.have.property('status').eql(403);
                    res.body.should.have.property('message').eql("Access Denied");
                    done();
                });
    })

    it('it should not authenticate when token is WRONG', (done) => {
        chai.request(server)
                .get('/api/v1/note')
                .set('Authorization', "khdhjfjhskjh")
                .end((err, res) => {
                    res.should.have.status(403);
                    res.body.should.be.a('Object');
                    res.body.should.have.property('status').eql(403);
                    res.body.should.have.property('message').eql("Access Denied");
                    done();
                });
    })
})