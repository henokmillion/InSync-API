let mongoose = require("mongoose");
let User = require('../models/user.model');
let fetch = require('node-fetch');
//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = "https://insync-api.herokuapp.com";
let should = chai.should();
let token = "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJub3RlcyI6W10sIl9pZCI6IjViMmI1ZjA5NDc4ZDkzMDAxZTlmNmFkNyIsImVtYWlsIjoiaGFoYUBnbWFpbC5jb20iLCJwYXNzd29yZCI6IiQyYSQxMCQxNmRhaS50RDQ0aDA4dHdLYS5YM1RPcS9GMDRtWXdVM3lkZGRHc0NsSG12SGNUL2NkZnJuNiIsImFjY291bnRTdGF0dXMiOiJhY3RpdmUiLCJfX3YiOjAsImlhdCI6MTUyOTU2OTA2OCwiZXhwIjoxNTI5NjU1NDY4fQ.AcVNhk_wIeat3N9gstwMXba4WrimG2A9xdZkKRkNS58";
let token2 = "Bearer2 eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJub3RlcyI6W10sIl9pZCI6IjViMmI1ZjA5NDc4ZDkzMDAxZTlmNmFkNyIsImVtYWlsIjoiaGFoYUBnbWFpbC5jb20iLCJwYXNzd29yZCI6IiQyYSQxMCQxNmRhaS50RDQ0aDA4dHdLYS5YM1RPcS9GMDRtWXdVM3lkZGRHc0NsSG12SGNUL2NkZnJuNiIsImFjY291bnRTdGF0dXMiOiJhY3RpdmUiLCJfX3YiOjAsImlhdCI6MTUyOTU2OTA2OCwiZXhwIjoxNTI5NjU1NDY4fQ.AcVNhk_wIeat3N9gstwMXba4WrimG2A9xdZkKRkNS58";

chai.use(chaiHttp);

describe('Login ', () => {

    describe('/login', () => {
        it('it should not let the user pass if the password is wrong', (done) => {
            let rand = Math.random() * 10000

            fetch(server + "/api/v1/user", {
                method: "POST",
                body: JSON.stringify({
                    "email": `picnic@time${rand}.com`,
                    "password": "picnic"
                }),
                headers: {
                    "Content-Type": "application/json; charset=UTF-8"
                }
            })
                .then(res => res.json())
                .then(response => {
                    let user = { email: `picnic@time${rand}.com`, password: "picnic1" }
                    chai.request(server)
                        .post('/api/v1/user/login')
                        .set("Content-Type", "application/json; charset=UTF-8")
                        .send(JSON.stringify(user))
                        .end((err, res) => {
                            res.should.have.status(403);
                            // res.error.text.should.have.property('message').eql('Authentication failed...');
                            done();
                        });
                })
        });
    });
});