let mongoose = require("mongoose");
let User = require('../models/user.model');
let fetch = require('node-fetch');
//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = "https://insync-api.herokuapp.com";
let should = chai.should();

let token = "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJub3RlcyI6W10sIl9pZCI6IjViMjgwNmYxMGY2ZDY5MDAxZTRlZTcwNiIsImVtYWlsIjoibWlsbGFAZ21haWwuY29tIiwicGFzc3dvcmQiOiIkMmEkMTAkL1FaL2JNdGk0b1NrZ0FOZW9rQWx4LmxsMnRRUDhXUzAvUTE2UTVTY0s0QkxFRFlGc3pNZy4iLCJhY2NvdW50U3RhdHVzIjoiYWN0aXZlIiwiX192IjowLCJpYXQiOjE1Mjk1OTc4OTV9.zP94lSTZLvJZrhGnZZkNH-L2_8kDq-enaaFTIisDbgM";

chai.use(chaiHttp);

describe('Users', () => {

    /*
    * Test the /GET route
    */
    describe('/GET user', () => {
        it('it should GET authenticated user info', (done) => {
            chai.request(server)
                .get('/api/v1/user/profile')
                .set("Authorization", token)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('Object');
                    res.body.should.have.property('data');
                    res.body.data.should.be.a('Object');
                    done();
                });
        });
    });
 
    //.......................................
    describe('/GET/:id user', () => {
        it('it should GET user by the given id', (done) => {
            let rand = Math.random() * 10000
            fetch(server + "/api/v1/user", {
                method: "POST",
                body: JSON.stringify({
                    "email": "picnic@time.com" + rand,
                    "password": "picnic"
                }),
                headers: {
                    "Content-Type": "application/json; charset=UTF-8"
                }
            })
                .then(res => res.json())
                .then(response => {
                    chai.request(server)
                        .get('/api/v1/user/' + response.data._id)
                        .end((err, res) => {
                            res.should.have.status(200);
                            res.body.should.be.a('object');
                            res.body.should.have.property('data');
                            res.body.data.should.have.property('_id')
                            res.body.data.should.have.property('email').eql("picnic@time.com" + rand);
                            res.body.data.should.have.property('password');
                            done();
                        });
                })
        });
    });

    //...........................................................


    describe('/POST user', () => {
        it('it should not POST a user without email, password', (done) => {
            let user = {
                email: "name@gmail.com"
            }
            chai.request(server)
                .post('/api/v1/user')
                .set("Authorization", token)
                .set("Content-Type", "application/json; charset=UTF-8")
                .send(user)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('msg').eql("Incomplete request body");
                    done();
                });
        });
    });
    
    //.................Post user with email and password..................
        describe('/POST user', () => {
        it('it should POST a user with email, password', (done) => {
            let rand = Math.random() * 10000            
            let user = {
                email: `eyerus${rand}@gmail.com`,
                password: "eyerus"
            }
            chai.request(server)
                .post('/api/v1/user')
                .set("Authorization", token)
                .set("Content-Type", "application/json; charset=UTF-8")
                .send(user)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('message').eql("successfully saved new user");
                    done();
                });
        });
    });

    //.........................................
    describe('/PUT/:id user', () => {
        it('it should UPDATE a user given the id', (done) => {
            let rand = Math.random() * 10000

            fetch(server + "/api/v1/user", {
                method: "POST",
                body: JSON.stringify({
                    "email": "picnic@time.com" + rand,
                    "password": "picnic"
                }),
                headers: {
                    "Content-Type": "application/json; charset=UTF-8"
                }
            })
                .then(res => res.json())
                .then(response => {
                    let user = { email: "guru@gmail.com"+rand, password: "jhad" }
                    chai.request(server)
                        .put('/api/v1/user/' + response.data._id)
                        .set("Authorization", token)
                        .set("Content-Type", "application/json; charset=UTF-8")
                        .send(JSON.stringify(user))
                        .end((err, res) => {
                            res.should.have.status(200);
                            res.body.should.be.a('object');
                            res.body.should.have.property('message').eql('Profile successfully updated.');
                            res.body.should.have.property('user')
                            res.body.user.should.have.property('email').eql('guru@gmail.com'+rand)
                            done();
                        });
                })
                .catch(err => console.error(err))
        });

        it('it should not UPDATE if a user doesnt give password', (done) => {
            let rand = Math.random() * 10000

            fetch(server + "/api/v1/user", {
                method: "POST",
                body: JSON.stringify({
                    "email": "picnic@time.com" + rand,
                    "password": "picnic"
                }),
                headers: {
                    "Content-Type": "application/json; charset=UTF-8"
                }
            })
                .then(res => res.json())
                .then(response => {
                    let user = { email: "picnic@gmail.com"+rand }
                    chai.request(server)
                        .put('/api/v1/user/' + response.data._id)
                        .set("Authorization", token)
                        .set("Content-Type", "application/json; charset=UTF-8")
                        .send(JSON.stringify(user))
                        .end((err, res) => {
                            res.should.have.status(400);
                            res.body.should.be.a('object');
                            res.body.should.have.property('status').eql(400)
                            res.body.should.have.property('msg').eql("Incomplete request body");
                            done();
                        });
                })
                .catch(err => console.error(err))
        });
    }); 
    //.............................

    /*
      * Test the /DELETE/:id route
      */
    describe('/DELETE/:id user', () => {
        it('it should DELETE a user given the id', (done) => {
            let rand = Math.random() * 10000

            fetch(server + "/api/v1/user", {
                method: "POST",
                body: JSON.stringify({
                    "email": "picnic@time.com" + rand,
                    "password": "picnic"
                }),
                headers: {
                    "Content-Type": "application/json; charset=UTF-8"
                }
            })
                .then(res => res.json())
                .then(response => {
                    chai.request(server)
                        .delete('/api/v1/user/' + response.data._id)
                        .set("Authorization", token)
                        .end((err, res) => {
                            res.should.have.status(200);
                            res.body.should.be.a('object');
                            res.body.should.have.property('message').eql('User successfully deleted');
                            res.body.should.have.property('user')
                            res.body.user.should.have.property('email').eql("picnic@time.com" + rand)
                            res.body.user.should.have.property('accountStatus').eql('inactive')
                            done();
                        });
                })
                .catch(err => console.error(err))
                
        });
    });



});


