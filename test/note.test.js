let mongoose = require("mongoose");
let Note = require('../models/note.model');
let fetch = require('node-fetch')
//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = "https://insync-api.herokuapp.com";
let should = chai.should();
let token = "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJub3RlcyI6W10sIl9pZCI6IjViMjgwNmYxMGY2ZDY5MDAxZTRlZTcwNiIsImVtYWlsIjoibWlsbGFAZ21haWwuY29tIiwicGFzc3dvcmQiOiIkMmEkMTAkL1FaL2JNdGk0b1NrZ0FOZW9rQWx4LmxsMnRRUDhXUzAvUTE2UTVTY0s0QkxFRFlGc3pNZy4iLCJhY2NvdW50U3RhdHVzIjoiYWN0aXZlIiwiX192IjowLCJpYXQiOjE1Mjk1OTc4OTV9.zP94lSTZLvJZrhGnZZkNH-L2_8kDq-enaaFTIisDbgM";

chai.use(chaiHttp);

describe('Notes', () => {

    /*
    * Test the /GET route
    */
    describe('/GET note', () => {
        it('it should GET all the notes', (done) => {
            chai.request(server)
                .get('/api/v1/note')
                .set("authorization", token)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('Object');
                    res.body.data.should.be.a('array');
                    res.body.should.have.property('status');
                    res.body.should.have.property('data');
                    done();
                });
        });
    });



    describe('/POST note', () => {
        it('it should not POST a note without title and body fields', (done) => {
            let note = {}
            chai.request(server)
                .post('/api/v1/note')
                .set("Authorization", token)
                .send(note)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.errors.should.have.property('body');
                    res.body.error.errors.should.have.property('title');
                    res.body.error.errors.body.should.have.property('kind').eql('required');
                    res.body.error.errors.title.should.have.property('kind').eql('required');
                    done();
                });
        });


        it('it should POST a note ', (done) => {
            let note = {
                title: "The Lord of the Rings",
                author: "J.R.R. Tolkien",
                body: "1954"
            }
            chai.request(server)
                .post('/api/v1/note')
                .send(note)
                .set("Authorization", token)
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('message').eql('successfully saved new note');
                    res.body.data.should.have.property('title');
                    res.body.data.should.have.property('author');
                    res.body.data.should.have.property('body');
                    done();
                });
        });
    });

    describe('/DELETE/:id note', () => {

        it('it should DELETE a note given the id', (done) => {
            fetch(server + "/api/v1/note", {
                method: "POST",
                body: JSON.stringify({
                    "title": "The Lord of the Rings",
                    "body": "1954"
                }),
                headers: {
                    "authorization": token,
                    "Content-Type": "application/json; charset=UTF-8"
                }
            })
                .then(res => res.json())
                .then(res => {
                    chai.request(server)
                        .delete('/api/v1/note/' + res.data._id)
                        .set("Authorization", token)
                        .end((err, res) => {
                            res.should.have.status(203);
                            res.body.should.be.a('object');
                            res.body.should.have.property('message').eql('successfully deleted note');
                            res.body.data.should.have.property('status').eql("deleted");
                            done();
                        });
                })
        })

        it('it should NOT DELETE a note given a WRONG id', (done) => {
            fetch(server + "/api/v1/note", {
                method: "POST",
                body: JSON.stringify({
                    "title": "The Lord of the Rings",
                    "body": "1954"
                }),
                headers: {
                    "authorization": token,
                    "Content-Type": "application/json; charset=UTF-8"
                }
            })
                .then(res => res.json())
                .then(res => {
                    chai.request(server)
                        .delete('/api/v1/note/' + 0)
                        .set("Authorization", token)
                        .end((err, res) => {
                            res.should.have.status(404);
                            res.body.should.be.a('object');
                            res.body.should.have.property('message').eql('Note not found');
                            done();
                        });
                })
        })


    })



    describe('/PUT/:id note', () => {
        it('it should UPDATE a note given the id', (done) => {
            fetch(server + "/api/v1/note", {
                method: "POST",
                body: JSON.stringify({
                    "title": "The Lord of the Rings",
                    "body": "1954"
                }),
                headers: {
                    "authorization": token,
                    "Content-Type": "application/json; charset=UTF-8"
                }
            })
                .then(res => res.json())
                .then(response => {
                    chai.request(server)
                        .put('/api/v1/note/' + response.data._id)
                        .send(
                            {
                                note: {
                                    title: "The Chronicles of Narnia",
                                    excerpt: "1950 ...",
                                    body: "1950 changed"
                                }
                            }
                        )
                        .set("Authorization", token)
                        .end((err, res) => {
                            // console.log(res)
                            res.should.have.status(200);
                            res.body.should.be.a('object');
                            res.body.should.have.property('message').eql('Profile successfully updated.');
                            res.body.note.should.have.property('body').eql("1950 changed");
                            done();
                        });
                })

        });





        it('it should NOT UPDATE a note given a WRONG id', (done) => {
            fetch(server + "/api/v1/note", {
                method: "POST",
                body: JSON.stringify({
                    "title": "The Lord of the Rings",
                    "body": "1954"
                }),
                headers: {
                    "authorization": token,
                    "Content-Type": "application/json; charset=UTF-8"
                }
            })
                .then(res => res.json())
                .then(response => {
                    chai.request(server)
                        .put('/api/v1/note/' + response.data._id + "rrr")
                        .send(
                            {
                                note: {
                                    title: "The Chronicles of Narnia",
                                    excerpt: "1950 ...",
                                    body: "1950 changed"
                                }
                            }
                        )
                        .set("Authorization", token)
                        .end((err, res) => {
                            res.should.have.status(404);
                            res.body.should.be.a('object');
                            res.body.should.have.property('message').eql("Note not found");
                            done();
                        });
                })

        });


    });
});


