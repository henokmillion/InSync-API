const express = require('express')
const User = require('../models/user.model')
const mongoose = require('mongoose')
const connection = require('./connection.service')

exports.getUsers = () => {
    return new Promise((succeed, fail) => {   
        User.find({
            accountStatus: "active"
        }, (err, user) => {
            if(err) {
                fail({
                    status: 404,
                    error: "User not found"
                })
            } else if (user){
                succeed({
                    status: 200,
                    data: user
                })
            } else {
                fail({
                    status: 404,
                    error: "User not found"
                })                
            }
        })
    })
}

exports.addUser = (user) => {
    return new Promise((succeed, fail) => {
        user = new User(user)
        user.save(err => {
            if (err) {
                fail({
                    status: 400,
                    message: "Unable to add user.",
                    error: err
                })
            } else {
                succeed({
                    status: 200,
                    message: "successfully saved new user",
                    data: user
                })
            }
        })
    })
}

exports.updateUser = user => {
    return new Promise((succeed, fail) => {
        User.findOne({
            _id: mongoose.Types.ObjectId(user.id),
            accountStatus: "active"
        }, (err, oldUser) => {
            if (err) {
                fail({
                    status: 501,
                    error: err
                })
            } else if(oldUser) {                
                oldUser.email = user.email,
                oldUser.password = user.password
                console.log(oldUser)
                
                oldUser.save((err, updated) => {
                    if (err) {
                        console.error(err)
                        fail({
                            status: 500,
                            error: err
                        })
                    } else {
                        succeed({
                            status: 200,
                            message: "Profile successfully updated.",
                            user: updated
                        })
                    }
                })
            } else {
                fail({
                    status: 404,
                    error: "user not found"
                })
            }
        })
    })
        
}

exports.getProfile = id => {
    return new Promise((succeed, fail) => {
        User.findById(id, (err, user) => {
            if(err) {
                fail({
                    status: 404,
                    message: "User not found"
                })
            } else {
                console.log(id)
                if (user.accountStatus === "active") {
                    succeed({
                        status: 200,
                        data: user
                    })
                } else {
                    fail({
                        status: 404,
                        message: "User not found"
                    })  
                }
            }
        })
    })
    
}

exports.terminateAccount = id => {
    return new Promise((succeed, fail) => {
        User.findById(id, (err, user) => {
            if (err) {
                fail({
                    status: 501,
                    err: err
                })
            } else if (user) {
                if (user.accountStatus !== "active") {
                    fail({
                        status: 400,
                        error: "account inactive"
                    })
                }
                user.accountStatus = "inactive"
                user.save(err => {
                    if (err) {
                        fail({
                            status: 501,
                            err: err
                        })
                    } else {
                        succeed({
                            status: 200,
                            message: "User successfully deleted",
                            user: user
                        })
                    }
                })
            } else {
                fail({
                    status: 404,
                    message: "User not found."
                })
            }
        })
    })
    
}

// save user
exports.save = user => {
    const _user = new User(user)
    console.log(user)
    return new Promise((succeed, fail) => {
        _user.save(err => {
            if (err) {
                fail({
                    status: 501,
                    error: new Error("unable to save user")
                })
            } else {
                succeed({
                    status: 200,
                    data: _user
                })
            }
        })
    })
}
