const mongoose = require('mongoose')
const config = require('../config')

exports.connection = (closure) => {
    return mongoose.connect(config.database, {useMongoClient: true}, (err, db) => {
        if (err) {
            return console.log(err)
        } else if (db) {
            closure(db)
        }
    })
}