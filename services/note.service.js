const express = require('express')
const Note = require('../models/note.model')
const mongoose = require('mongoose')
const connection = require('./connection.service')

exports.getNotes = (_author) => {
    return new Promise((succeed, fail) => {
        Note.find({
            "author._id": _author._id,
            status: 'active'
        }, (err, notes) => {
            if (err) {
                fail({
                    status: 404,
                    msg: "no notes found"
                })
            } else if (notes) {
                succeed({
                    status: 200,
                    data: notes
                })
            }
        })
    })
}

exports.deleteNote = (id, _author) => {
    console.log(id)
    return new Promise((succeed, fail) => {
        if (!mongoose.Types.ObjectId.isValid(id)) {
            fail({
                status: 404,
                message: "Note not found"
            })
        }
        Note.findOne({
            _id: mongoose.Types.ObjectId(id),
            "author._id": _author._id,
            status: "active"
        }, (err, note) => {
            console.log(id)
            if (err) {
                fail({
                    status: 500,
                    message: "Unable to delete note",
                    error: err
                })
            } else if (note) {
                note.status = 'deleted'
                note.save(err => {
                    if (err) {
                        fail({
                            status: 500,
                            message: "Unable to delete note",
                            error: err
                        })
                    } else if (note) {
                        succeed({
                            status: 203,
                            message: "successfully deleted note",
                            data: note
                        })
                    } else {
                        fail({
                            status: 404,
                            message: "note not found"
                        })
                    }
                })
            } else {
                fail({
                    status: 404,
                    message: "Note not found"
                })
            }
        })
    })
}

exports.addNote = (note) => {
    return new Promise((succeed, fail) => {
        note = new Note(note)
        note.save(err => {
            if (err) {
                fail({
                    status: 400,
                    message: "Unable to add note.",
                    error: err
                })
            } else {
                succeed({
                    status: 201,
                    message: "successfully saved new note",
                    data: note
                })
            }
        })
    })
}

exports.updateNote = (id, note) => {
    return new Promise((succeed, fail) => {
        if (!mongoose.Types.ObjectId.isValid(id)) {
            fail({
                status: 404,
                message: "Note not found"
            })
        }
        Note.findOne({
            _id: mongoose.Types.ObjectId(id),
            status: "active"
        }, (err, oldnote) => {
            if (err) {
                fail({
                    status: 501,
                    error: err
                })
            } else if (oldnote) {


                oldnote.title = note.title
                oldnote.body = note.body

                oldnote.save((err, updated) => {
                    if (err) {
                        fail({
                            status: 500,
                            error: err
                        })
                    } else {
                        succeed({
                            status: 200,
                            message: "Profile successfully updated.",
                            note: updated
                        })
                    }
                })
            } else {
                fail({
                    status: 404,
                    error: "note not found"
                })
            }
        })
    })
}

exports.save = note => {
    const _note = new Note(note)
    console.log(note)
    return new Promise((succeed, fail) => {
        _note.save(err => {
            if (err) {
                fail({
                    status: 501,
                    error: new Error("unable to save note")
                })
            } else {
                succeed({
                    status: 201,
                    data: _note
                })
            }
        })
    })
}
