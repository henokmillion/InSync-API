const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const config = require('../config')
const User = require('../models/user.model')
const jwt = require('jsonwebtoken')

// import middlewares
const authMiddleware = require('../middleware/auth.middleware')

// get the user controller ok
const userController = require('../controllers/user.controller')
const userRoute = new express.Router()
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))

app.set('secret', config.secret)

mongoose.connect(config.database, {useMongoClient: true})

// get personal profile info.
userRoute.get('/profile', authMiddleware.authMiddleware, userController.getProfile)

// get user info
userRoute.get('/:id', userController.getUsers)

// create user
userRoute.post('/', userController.addUser)

userRoute.use(authMiddleware.authMiddleware)

// terminate account
userRoute.delete('/:id', userController.terminateAccount)

// edit personal profile
userRoute.put('/:id', userController.updateUser)

module.exports = userRoute