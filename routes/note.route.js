const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const config = require('../config')
const Note = require('../models/note.model')
const jwt = require('jsonwebtoken')

// import middlewares
const authMiddleware = require('../middleware/auth.middleware')

// get the user controller ok
const noteController = require('../controllers/note.controller')
const noteRoute = new express.Router()
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))

app.set('secret', config.secret)

mongoose.connect(config.database, {useMongoClient: true})

noteRoute.use(authMiddleware.authMiddleware)

// get notes
noteRoute.get('/', noteController.getNotes)

// create note
noteRoute.post('/', noteController.addNote)

// delete note
noteRoute.delete('/:id', noteController.deleteNote)

// edit note
noteRoute.put('/:id', noteController.updateNote)



module.exports = noteRoute
